# yAPI
![](https://img.shields.io/github/languages/code-size/exoad/yAPI) ![](https://img.shields.io/github/repo-size/exoad/yAPI) [![CMake](https://github.com/exoad/yAPI/actions/workflows/cmake.yml/badge.svg)](https://github.com/exoad/yAPI/actions/workflows/cmake.yml) [![CodeQL](https://github.com/exoad/yAPI/actions/workflows/codeql-analysis.yml/badge.svg)](https://github.com/exoad/yAPI/actions/workflows/codeql-analysis.yml) [![Docker Image CI](https://github.com/exoad/yAPI/actions/workflows/docker-image.yml/badge.svg)](https://github.com/exoad/yAPI/actions/workflows/docker-image.yml)

This is a Discord Bot I made along with an internal API (thats why its called YttriusAPI or yAPI)

`/core` is mainly used for one of the C libraries I use to parse JS.

This folder does not only include the bot, but also includes its API.

Invite it [here](https://discord.com/oauth2/authorize?client_id=871572127806017627&permissions=3154508918&scope=bot)

**THIS IS A BITBUCKET IMPORT OF YAPI AND IS NOT UPDATED REGULARLY PLEASE REFER EITHER TO GITLAB OR GITHUB**